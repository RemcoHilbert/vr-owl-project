﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviour : MonoBehaviour
{

    public float thrust;
    public Rigidbody projectile;
    public GameObject pointer;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //BASKET SHOOT MECHANIC
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 position = pointer.transform.position;
            position = Camera.main.ScreenToWorldPoint(position);

            Rigidbody clone;
            clone = Instantiate(projectile, transform.position, transform.rotation) as Rigidbody;
            clone.velocity = transform.TransformDirection(Vector3.forward * 10);
        }
    }


    //OLD MECHANIC, NOT USED
    public void BallHit()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, -Vector3.up, out hit))
        {
            print("Found an object - distance: " + hit.distance);
           // Vector3 theVector = ht.normalized;

            this.gameObject.GetComponent<Rigidbody>().AddForce(hit.point * thrust);
        }
        //    if (Physics.Raycast(ray, out hit))
        //    {
        //        Transform objectHit = hit.transform;
        //        Debug.Log("Hit Transform: " + objectHit.position);
        //        this.gameObject.GetComponent<Rigidbody>().AddRelativeForce(objectHit.position + Vector3.forward * thrust);
        //    }   
    }
}